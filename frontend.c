#include <ncurses.h>
#include <stdlib.h>
#include "objs.h"

int dy = 1, dx = 1;

void
move_ball(struct objs *ball)
{
    ball->x += dx;
    ball->y += dy;

    mvaddch(ball->y, ball->x, ball->icon);
}


void
draw_bricks()
{
    struct objs *tmp = head;
    while (tmp != NULL) {
        mvaddch(tmp->y, tmp->x, tmp->icon);
        tmp = tmp->next; 
    }
}

void
draw_slider(struct objs *slider)
{
    for (int i = 0; i < SLIDER_LENGTH; i++)
        mvaddch(slider->y, slider->x + i, slider->icon); // s->y SAME (*s).y
    
}


